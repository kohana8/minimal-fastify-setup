const path = require('path');
const session = require('@komino/fastify-session');
const SQLiteStore = require('connect-better-sqlite3')(session);

class Server{
  constructor(fastify = null, argOptions= {}){
    const options = Object.assign({
      salt: 'saltForSignatureLongerIsBetterAtLeast32lettersLong',
      secureCookie: false,
      maxAge: 86400000,
      sessionDBFolder: path.join(__dirname, '../db'),
      logger: false,
    }, argOptions);

    const app = fastify || require('fastify')({
      logger: options.logger,
      ignoreTrailingSlash: true
    });

    // Require the framework and instantiate it
    app.register(require('fastify-helmet'));
    app.register(require('fastify-cookie'));
    app.register(session, {
      secret : options.salt,
      cookie : {
        secure: options.secureCookie,
        maxAge: options.maxAge,
      },
      saveUninitialized : false,
      store : new SQLiteStore({
        dir: options.sessionDBFolder,
      })
    });

    app.register(require('fastify-formbody'));
    app.register(require('fastify-multipart'));

    this.app = app;
  }

  // Run the server!
  async listen(port = 8081){
    try {
      await this.app.listen(port);
      console.log('listening:'+ port);
    } catch (err) {
      this.app.log.error(err);
      process.exit(1);
    }
  }
}

module.exports = Server;